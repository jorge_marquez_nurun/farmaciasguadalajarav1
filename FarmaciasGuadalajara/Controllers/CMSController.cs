﻿using FarmaciasGuadalajara.AppObjects;
using FarmaciasGuadalajara.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.Security;

namespace FarmaciasGuadalajara.Controllers
{
    public class CMSController : Controller
    {
        FGEntities db = new FGEntities();
        //cs_Admin cl = new cs_Admin();

        // GET: CMS
        public ActionResult j0enHdx4Edx()
        {
            try {

                if (new cs_Admin().getId() > 0) {
                    var menu = new MenuAdmin();
                    var a = menu;
                    return RedirectToAction("dashBoard");
                } else {
                    return View();
                }

                
            } catch (Exception ex) {
                return View();
            }
        }

        // GET: CMS
        public ActionResult exj0enHdx4Edxquj4mj() {
            try {
                if (new cs_Admin().getId() > 0) {
                    db.sp_change();
                    return View();
                } else {
                    return RedirectToAction("j0enHdx4Edx");
                }
            } catch (Exception ex) {
                return View();
            }
        }

        // GET: CMS
        public ActionResult dashBoard() {
            try {
                if (new cs_Admin().getId() > 0) {
                    var data = db.sp_registros().ToList();
                    ViewBag.Data = db.sp_registros().First();
                    return View();
                } else {
                    return RedirectToAction("j0enHdx4Edx");
                }
            } catch (Exception ex) {
                return View();
            }
        }

        // GET: CMS
        public ActionResult clientList() {
            try {

                var l = new cs_Admin().getId();

                if (new cs_Admin().getId() == 0) {
                    return RedirectToAction("j0enHdx4Edx");
                } else {
                    ViewBag.Menu = new MenuAdmin().Menuelement;
                    var data = (cs_Result)getClientList(10,1).Data;                    
                    if (data.code == 0) {
                        var cl = (from elements in JsonConvert.DeserializeObject<List<Cat_Client>>(JArray.Parse(data.message).ToString())
                                  select new Cat_Client {
                                      ClientId = elements.ClientId,
                                      Date = elements.Date,
                                      Name = elements.Name + " " + elements.LastName1 + " " + elements.LastName2,
                                      Email = elements.Email,
                                      LadaCelular = elements.LadaCelular,
                                      Phone = elements.Phone,
                                      Status = elements.Status
                                  }).ToList();
                        ViewBag.TotalPage = cl.First().Status;
                        return View(cl);
                    }

                    ViewBag.TotalPage = null;

                    return View();
                }

            } catch (Exception ex) {
                return View();
            }
        }

        //*************** SHOW MODAL
        [HttpPost]
        public JsonResult getClientList(Int32 RowsPerPage, Int32 PageNumber) {
            try {
                var data = db.sp_getClientList(RowsPerPage, PageNumber).First();
                var cl = (from elements in JsonConvert.DeserializeObject<List<Cat_Client>>(JArray.Parse(data.message.ToString()).ToString())
                          select new {
                              ClientId = elements.ClientId,
                              Date = elements.Date,
                              Name = elements.Name + " " + elements.LastName1 + " " + elements.LastName2,
                              Email = SimpleEncryption.Decrypt(elements.Email, "FG"),
                              LadaCelular = SimpleEncryption.Decrypt(elements.LadaCelular, "FG"),
                              Phone = SimpleEncryption.Decrypt(elements.Phone, "FG"),
                              Status = elements.Status
                          }).ToList();
                var a = JsonConvert.SerializeObject(cl);
                return Json(new cs_Result(0, JsonConvert.SerializeObject(cl)), JsonRequestBehavior.AllowGet);
            } catch (Exception ex) {
                return Json(new cs_Result(1, ex.Message), JsonRequestBehavior.AllowGet);
            }
        }

        // GET: CMS
        public ActionResult best_results() {
            try {

                var l = new cs_Admin().getId();

                if (new cs_Admin().getId() == 0) {
                    return RedirectToAction("j0enHdx4Edx");
                } else {
                    ViewBag.Menu = new MenuAdmin().Menuelement;
                    var data = (cs_Result)getClientListWinners(10, 1).Data;
                    if (data.code == 0) {
                        var cl = (from elements in JsonConvert.DeserializeObject<List<cs_Winners>>(JArray.Parse(data.message.ToString()).ToString())
                                  select new {
                                      Lugar = elements.Lugar,
                                      Nombre = elements.Nombre,
                                      Aciertos = elements.Aciertos,
                                      Tiempo = elements.Tiempo,
                                      TotalPage = elements.TotalPage
                                  }).ToList();
                        var a = JsonConvert.SerializeObject(cl);

                        ViewBag.TotalPage = cl.First().TotalPage;
                        return View(cl);
                    }

                    ViewBag.TotalPage = null;

                    return View();
                }

            } catch (Exception ex) {
                return View();
            }
        }

        //*************** SHOW MODAL
        [HttpPost]
        public JsonResult getClientListWinners(Int32 RowsPerPage, Int32 PageNumber) {
            try {
                var data = db.sp_ticketResultWinners(RowsPerPage, PageNumber).First();
                var cl = (from elements in JsonConvert.DeserializeObject<List<cs_Winners>>(JArray.Parse(data.message.ToString()).ToString())
                          select new {
                              Lugar = elements.Lugar,
                              Nombre = elements.Nombre,
                              Aciertos = elements.Aciertos,
                              Tiempo = elements.Tiempo,
                              TotalPage = elements.TotalPage
                          }).ToList();
                var a = JsonConvert.SerializeObject(cl);
                return Json(new cs_Result(0, JsonConvert.SerializeObject(cl)), JsonRequestBehavior.AllowGet);
            } catch (Exception ex) {
                return Json(new cs_Result(1, ex.Message), JsonRequestBehavior.AllowGet);
            }
        }

        // GET: CMS
        public ActionResult adminList() {
            try {
                return View();
            } catch (Exception ex) {
                return View();
            }
        }
        
        //*************** SHOW MODAL
        [HttpPost]
        public ActionResult popData(string option) {
            try {
                ViewBag.data = option;
                return View();
            } catch (Exception ex) {
                return View();
            }
        }

        //**************** CUSTOMER PROCESS (CRUD)
        [HttpPost]
        public JsonResult adminProcess(Cat_Admin admin, string option) {
            try {
                var user = SimpleEncryption.Encrypt(admin.UserName, "FG");
                var pass = SimpleEncryption.Encrypt(admin.Password, "FG");

                var resul = db.sp_AdminCRUD(option, Convert.ToInt32(admin.AdminId), admin.Name, admin.LastName, user, pass, admin.Status).First();
                if (option == "C" || option == "R") {
                    if (resul.code == 0) {
                        var obj = JObject.Parse(resul.message.ToString());
                        Session["Admin"] = new cs_Admin(Convert.ToInt32(obj["AdminId"].ToString()), obj["Name"].ToString());
                        resul.message = obj["Name"].ToString();

                        FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket(obj["AdminId"].ToString(), true, 12 * 60);
                        string encryptedTicket = FormsAuthentication.Encrypt(authTicket);
                        HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);

                        if (HttpContext.Request.IsSecureConnection) { cookie.Secure = true; }
                        cookie.HttpOnly = true;

                        cookie.Expires = authTicket.Expiration;
                        System.Web.HttpContext.Current.Response.Cookies.Set(cookie);

                    }
                }
                return Json(Json(resul).Data);
            } catch (Exception ex) {
                return Json(new cs_Result(1, ex.Message), JsonRequestBehavior.AllowGet);
            }
        }

        //**************** CUSTOMER PROCESS (CRUD)
        [HttpPost]
        public JsonResult customerProcess(Cat_Client client, string option) {
            try {
                var email = "";
                var pass = "";
                var lada = "";
                var phone = "";

                if (client.ClientId == 0) {
                    email = SimpleEncryption.Encrypt(client.Email, "FG");
                    pass = SimpleEncryption.Encrypt(client.Password, "FG");
                }

                if (option == "U") {
                    email = SimpleEncryption.Encrypt(client.Email, "FG");
                    pass = SimpleEncryption.Encrypt(client.Password, "FG");
                    lada = SimpleEncryption.Encrypt(client.LadaPhone, "FG");
                    phone = SimpleEncryption.Encrypt(client.Phone, "FG");
                }

                Cat_Client cl = new Cat_Client();
                var resul = db.sp_clienteCRUD(option, Convert.ToInt32(client.ClientId), client.Name, client.LastName1, client.LastName2, email, pass, lada, phone, client.LadaCelular, client.Celular, client.TermsConditions, client.NoticePrivacy, client.Status).First();
                if (option == "R") {
                    cl = JsonConvert.DeserializeObject<Cat_Client>(JObject.Parse(resul.message.ToString()).ToString());

                    cl.Email = SimpleEncryption.Decrypt(cl.Email, "FG");
                    cl.Password = SimpleEncryption.Decrypt(cl.Password, "FG");
                    cl.LadaPhone = SimpleEncryption.Decrypt(cl.LadaPhone, "FG");
                    cl.Phone = SimpleEncryption.Decrypt(cl.Phone, "FG");

                    JavaScriptSerializer js = new JavaScriptSerializer();
                    resul.message = js.Serialize(cl).Replace(",\"Tbl_Transaction\":[]", "");

                }
                return Json(Json(resul).Data);
            } catch (Exception ex) {
                return Json(new cs_Result(1, ex.Message), JsonRequestBehavior.AllowGet);
            }
        }

        //**************** CUSTOMER PROCESS (CRUD)
        [HttpPost]
        public JsonResult clientTicketResultList(Int32 ClienteId) {
            try {                
                var resul = db.sp_clientTicket(ClienteId).First();
                return Json(Json(resul).Data);
            } catch (Exception ex) {
                return Json(new cs_Result(1, ex.Message), JsonRequestBehavior.AllowGet);
            }
        }

        //*************** SHOW MODAL
        [HttpPost]
        public void logOut() {
            Session.Clear();
            FormsAuthentication.SignOut();
        }

        /**************************************
         * CLOSE CONEXTION TO DATA BASE
         *************************************/
        protected override void Dispose(bool disposing) {
            if (disposing) {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}