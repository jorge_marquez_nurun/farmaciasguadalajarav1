﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FarmaciasGuadalajara.Models;
using FarmaciasGuadalajara.AppObjects;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Web.Script.Serialization;
using System.Net;
using System.Web.Security;
using System.Text.RegularExpressions;

namespace FarmaciasGuadalajara.Controllers
{
    public class MainController : Controller
    {
        FGEntities db = new FGEntities();
        cs_Client cl = new cs_Client();

        // GET: Main
        public ActionResult Index()
        {
            ViewBag.Menu = null;
            ViewBag.Result = null;
            ViewBag.winners = null;

            ////////var url = new ObjURL().getURLData(Request.Url.ToString());
            ////////var beforTable = "<p><b>Jorge Marquez Juarez</b>, agradecemos tu preferencia.</p>";
            ////////var Html1 = "<img src='" + url.UrlImage + @"/img/fondoEmail.png'>";

            ////////new cs_Email("noreply@kelloggs.com", "Kellogg’s® Cumple tus deseos Navideños", "jorgejamj@gmail.com", "", "Kellogg’s® Cumple tus deseos Navideños - Jorge Marquez Juarez", Html1, true, new ObjURL().getURLData(Request.Url.ToString()), beforTable).sendMail();

            var dummy = new cs_Client().getId();
            var dummyx = "";
            try {
                var configuration = new cs_Configuration();
                ViewBag.Menu = getMenu();
                ViewBag.Result = configuration;
                ViewBag.Analytics = configuration.Analytics;

                if (configuration.status == 3) {

                    var a = db.sp_ticketResultWinners(15,1).ToList();

                    ViewBag.winners = db.sp_ticketResultWinners(15, 1).ToList();
                }
                return View();
            } catch (Exception ex) {
                return View();
            }
        }

        public ActionResult Register() {
            ViewBag.Menu = null;
            ViewBag.Result = null;

            try {
                if (new cs_Client().getId() > 0) {
                    return RedirectToAction("Index");
                }
                var configuration = new cs_Configuration();
                ViewBag.Menu = getMenu();
                ViewBag.Result = configuration;
                ViewBag.Analytics = configuration.Analytics;
                ViewBag.ShowAnalytics = configuration.ShowAnalytics;

                return View();
            } catch (Exception ex) {
                return View();
            }
        }

        
        public ActionResult Gracias() {
            ViewBag.Menu = null;
            ViewBag.Result = null;

            try {
                //if (new cs_Client().getId() > 0) {
                //    return RedirectToAction("Index");
                //}
                var configuration = new cs_Configuration();
                ViewBag.Menu = getMenu();
                ViewBag.Result = configuration;

                return View();
            } catch (Exception ex) {
                return View();
            }
        }

        public ActionResult profile() {

            try {
                var ClientId = new cs_Client().getId();
                ViewBag.Menu = getMenu();
                if (ClientId == 0) {
                    return RedirectToAction("Index");
                }

                var a = db.sp_ticketResultClient(Convert.ToInt32(ClientId)).ToList();

                return View(db.sp_ticketResultClient(Convert.ToInt32(ClientId)).ToList());
            } catch (Exception ex) {
                return RedirectToAction("Index");
            }
        }

        [HttpPost]
        public string profileTickt(Int32 ticketId) {
            Session["TicketId"] = ticketId;
            return "Ok";
        }

        public ActionResult ChristmasTest() {

            try {
                if (new cs_Client().getId() == 0) {
                    return RedirectToAction("Index");
                }

                ViewBag.getState = getState();
                ViewBag.InitQuestion = null;
                //var InitQuestion = null;
                
                if (Session["TicketId"] != null) {
                    Tbl_Ticket tk = new Tbl_Ticket();
                    tk.TicketId = Convert.ToInt32(Session["TicketId"]);
                    db.sp_ticketCRUD("IT", tk.TicketId, Convert.ToInt32(new cs_Client().getId()),"", 0, 0, 1).First();
                    ViewBag.InitQuestion = JsonConvert.DeserializeObject<QuestionElement>(((sp_ticketCRUD_Result)ticketProcess(tk, "R").Data).message.ToString());
                } else {
                    var ticketPendiente = db.sp_getTicketPending(Convert.ToInt32(new cs_Client().getId())).First();
                    if (ticketPendiente.ToString() != "0") {
                        var tick = new Tbl_Ticket();
                        tick.TicketId = Convert.ToInt32(ticketPendiente);
                        ViewBag.InitQuestion = JsonConvert.DeserializeObject<QuestionElement>(((sp_ticketCRUD_Result)ticketProcess(tick, "R").Data).message.ToString());
                    }
                }

                ViewBag.Menu = getMenu();

                return View();
            } catch (Exception ex) {
                return RedirectToAction("Index");
            }
        }

        public ActionResult Award() {

            //if (new Client().getId() == 0) {
            //    return RedirectToAction("Index");
            //}

            ViewBag.Menu = getMenu();
            return View();

        }

        public ActionResult Winners() {

            //if (new Client().getId() == 0) {
            //    return RedirectToAction("Index");
            //}

            ViewBag.Menu = getMenu();
            return View();

        }

        //*************** GET OPTIONS OF MENU
        public string getMenu() {
            var menu = new Menu(0).menu;
            try {
                if (new cs_Client().getId() > 0) {
                    menu = "";
                    menu = new Menu(1).menu;
                }
            } catch (Exception es) { }
            return menu;
        }

        //*************** SHOW MODAL
        [HttpPost]
        public ActionResult popData(string option) {
            try {
                ViewBag.data = option;
                return View();
            } catch (Exception ex) {
                return View();
            }
        }
        
        static string validatePass(string strIn) {
            // Replace invalid characters with empty strings.
            try {
                return Regex.Replace(strIn, @"[^\w\$@$!%*._]", "", RegexOptions.None, TimeSpan.FromSeconds(1.5));
            }
            // If we timeout when replacing invalid characters, 
            // we should return Empty.
            catch (RegexMatchTimeoutException) {
                return String.Empty;
            }

        }

        static bool validateEmail(string strIn) {
            // Replace invalid characters with empty strings.
            try {
                Regex Val = new Regex(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*");
                if (Val.IsMatch(strIn)) {
                    return true;
                }
                return false;
            }
            // If we timeout when replacing invalid characters, 
            // we should return Empty.
            catch (RegexMatchTimeoutException) {
                return false;
            }

        }

        //**************** CUSTOMER PROCESS (CRUD)
        [HttpPost]
        public JsonResult customerProcess(Cat_Client client, string option) {
            try {
                
                var errors = "";
                var email = "";
                var pass = "";
                var ladaPhone = "";
                var phone = "";
                var rnd = new GeneradorPassword();
                    rnd.LongitudPassword = 16;
                    rnd.PorcentajeMayusculas = 25;
                    rnd.PorcentajeNumeros = 25;
                    rnd.PorcentajeSimbolos = 25;
                    rnd.GetNewPassword();
                var newPass = rnd.GetNewPassword().ToString();

                switch (option) {
                    case "C":
                        if (!validateEmail(client.Email)) {
                            errors = errors + "<p>Formato invalido para el campo <b>Email</b></p>";
                        }
                        if (validatePass(client.Password) != client.Password) {
                            errors = errors + "<p>La contraseña debe tener al entre <b>8</b> y <b>16</b> caracteres</p> <p>Al menos un dígito, al menos una minúscula y al menos una mayúscula.</p> <p>Símbolos permitidos <b>$@!% *._</b>.</p><p>Ejemplo: <b>M1c0ntr@señ4.01</b></p>";
                        }
                        email = SimpleEncryption.Encrypt(client.Email, "FG");
                        pass = SimpleEncryption.Encrypt(client.Password, "FG");
                        ladaPhone = SimpleEncryption.Encrypt(client.LadaPhone, "FG");
                        phone = SimpleEncryption.Encrypt(client.Phone, "FG");
                        break;
                    case "R":
                        if (!validateEmail(client.Email)) {
                            errors = errors + "<p>Formato invalido para el campo <b>Email</b></p>";
                        }
                        if (validatePass(client.Password) != client.Password) {
                            errors = errors + "<p>La contraseña debe tener al entre <b>8</b> y <b>16</b> caracteres</p> <p>Al menos un dígito, al menos una minúscula y al menos una mayúscula.</p> <p>Símbolos permitidos <b>$@!% *._</b>.</p><p>Ejemplo: <b>M1c0ntr@señ4.01</b></p>";
                        }
                        email = SimpleEncryption.Encrypt(client.Email, "FG");
                        pass = SimpleEncryption.Encrypt(client.Password, "FG");
                        break;
                    case "RE":
                        email = SimpleEncryption.Encrypt(client.Email, "FG");
                        pass = SimpleEncryption.Encrypt(newPass, "FG");
                        break;
                }

                if (errors != "") {
                    var a = Json(new cs_Result(1, "{\"message\":\"Error al generar la solicitud.\", \"reason\":\"" + errors + ".\"}"), JsonRequestBehavior.AllowGet);
                    return Json(new cs_Result(1, "{\"message\":\"" + errors + ".\", \"reason\":\"" + errors + ".\"}"), JsonRequestBehavior.AllowGet);
                }

                var data = "sp_clienteCRUD " + option + "," + client.ClientId + "," + client.Name + "," + client.LastName1 + "," + client.LastName2 + "," + email + "," + pass + "," + ladaPhone + "," + phone + "," + client.LadaCelular + "," + client.Celular + "," + client.TermsConditions + "," + client.NoticePrivacy + "," + client.Status;

                var resul = db.sp_clienteCRUD(option, Convert.ToInt32(client.ClientId), client.Name, client.LastName1, client.LastName2, email, pass, ladaPhone, phone, client.LadaCelular, client.Celular, client.TermsConditions, client.NoticePrivacy, client.Status).First();
                if (option == "C" || option == "R" || option == "RE") {
                    if (resul.code == 0) {
                        //var obj = JObject.Parse(resul.message.ToString());

                        var dataResult = Newtonsoft.Json.JsonConvert.DeserializeObject<Cat_Client>(resul.message.ToString());

                        switch (option) {
                            case "C":
                                Session["Client"] = new cs_Client(dataResult);
                                resul.message = dataResult.Name+" "+ dataResult.LastName1 + " " + dataResult.LastName2;

                                var url = new ObjURL().getURLData(Request.Url.ToString());
                                var beforTable = ""; //"<p><b>"+ resul.message + "</b>, agradecemos tu preferencia.</p>";

                                var Html = @"<table border='0' cellpadding='0' cellspacing='0' style='color:#FFFFFF' bgcolor='#037EC1' background='font-family: KelloggsSans, KelloggsSans, Open Sans, Optima, Verdana, sans-serif;'>
                                                <tr>
                                                    <td colspan='2'><h1 color='#FFFFFF ;'>CONFIRMACIÓN DE REGISTRO</h1></td>
                                                </tr>
                                                <tr>
                                                    <td colspan='2'>
                                                        <h2 color='#FFFFFF ;'>¡Hola!, <span style='color:#f8e71c ;'>" + client.Name + " " + client.LastName1 + " " + client.LastName2 + @"</span></h2>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                    	                                <p><h3 color='#FFFFFF ;'>Tu registro <span style='color:#f8e71c ;'>fue exitoso</span></h3></p>
                        
                                                        <p><h3 color='#FFFFFF ;'><span style='color:#f8e71c ;'>Sigue participando</span> para aumentar tus posibilidades de ganar.</h3></p>
                        
                                                        <p><h3 color='#FFFFFF ;'>Recuerda que tenemos grandes <span style='color:#f8e71c ;'>premios en monedero electrónico</span>.</h3></p>
                        
                                                        <p><h3 color='#FFFFFF ;'>Este correo es sólo de <span style='color:#f8e71c ;'>confirmación</span>.</h3></p>
                                                    </td>
                                                </tr>
                                            </table>";

                                //new cs_Email("noreply@kelloggs.com", "Kellogg’s® Cumple tus deseos Navideños", "jorgejamj@gmail.com", "", "Kellogg’s® Cumple tus deseos Navideños - Tu nueva contraseña de acceso.", html, true, new ObjURL().getURLData(Request.Url.ToString()), "").sendMail();
                                new cs_Email("noreply@kelloggs.com", "Kellogg’s® Cumple tus deseos Navideños", client.Email, "", "Kellogg’s® Cumple tus deseos Navideños - "+ resul.message, Html, true, new ObjURL().getURLData(Request.Url.ToString()), beforTable).sendMail();
                                
                                FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket(client.ClientId.ToString(), true, 12 * 60);
                                string encryptedTicket = FormsAuthentication.Encrypt(authTicket);
                                HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);

                                if (HttpContext.Request.IsSecureConnection) { cookie.Secure = true; }
                                cookie.HttpOnly = true;

                                cookie.Expires = authTicket.Expiration;
                                System.Web.HttpContext.Current.Response.Cookies.Set(cookie);
                                
                                break;
                            case "R":
                                Session["Client"] = new cs_Client(dataResult);
                                resul.message = dataResult.Name + " " + dataResult.LastName1 + " " + dataResult.LastName2;

                                FormsAuthenticationTicket authTicket1 = new FormsAuthenticationTicket(client.ClientId.ToString(), true, 12 * 60);
                                string encryptedTicket1 = FormsAuthentication.Encrypt(authTicket1);
                                HttpCookie cookie1 = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket1);

                                if (HttpContext.Request.IsSecureConnection) { cookie1.Secure = true; }
                                cookie1.HttpOnly = true;

                                cookie1.Expires = authTicket1.Expiration;
                                System.Web.HttpContext.Current.Response.Cookies.Set(cookie1);

                                break;
                            case "RE":
                                var HtmlP = @"<table border='0' cellpadding='0' cellspacing='0' style='color:#FFFFFF' bgcolor='#037EC1' background='font-family: KelloggsSans, KelloggsSans, Open Sans, Optima, Verdana, sans-serif;'>
                                                <tr>
                                                    <td colspan='2'>
                                                        <p><h2 color='#FFFFFF ;'>¡Hola!, tu nueva contraseña para acceder es:</p></br><span style='color:#f8e71c ;'><b><h2>" + newPass + @"</h2></b></span></h2>
                                                    </td>
                                                </tr>
                                            </table>";
                                new cs_Email("noreply@kelloggs.com", "Kellogg’s® Cumple tus deseos Navideños", client.Email, "", "Kellogg’s® Cumple tus deseos Navideños - Tu nueva contraseña de acceso.", HtmlP, true, new ObjURL().getURLData(Request.Url.ToString()), "").sendMail();
                                break;
                        }
                    }
                }
                return Json(Json(resul).Data);
            } catch (Exception ex) {
                return Json(new cs_Result(1, "{\"message\":\"Error al generar la solicitud.\", \"reason\":\"" + ex.Message+".\"}"), JsonRequestBehavior.AllowGet);
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult ManageReCaptcha() {
            var response = Request["g-recaptcha-response"]; string secretKey = "6LdL2zcUAAAAALjYcgEc5Zyl6SnvJzLM1kw_Y6v2";
            var client = new WebClient();
            var result = client.DownloadString(string.Format("https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}", secretKey, response));
            return Json(result);
        }

        //**************** TICKET PROCESS (CRUD)
        [HttpPost]
        public JsonResult ticketProcess(Tbl_Ticket ticket, string option) {
            try {

                Session["TicketId"] = null;
                Session["Question"] = null;
                var a = new cs_Client().getId();
                var resul = db.sp_ticketCRUD(option, Convert.ToInt32(ticket.TicketId), Convert.ToInt32(new cs_Client().getId()), ticket.SerialNumber, ticket.SucursalNumber, Convert.ToInt32(ticket.Total), 1).First();
                if (option == "C" || option == "R") {
                    var obj = JObject.Parse(resul.message.ToString());
                    if (resul.code == 0) {
                        var cla = new cs_Client().getId();
                        var resultQuestion = db.sp_question_answer_JSON(Convert.ToInt32(new cs_Client().getId())).First();
                        if (resultQuestion.code == 0) {
                            Session["TicketId"] = obj["TicketId"];
                            var question = new cs_Question(JsonConvert.DeserializeObject<List<QuestionElement>>(resultQuestion.message)).questionSearch();
                            resul.message = new JavaScriptSerializer().Serialize(question).ToString();
                        } else {
                            resul.code = resultQuestion.code;
                            resul.message = resultQuestion.message;
                        }
                    } else {
                        resul.message = obj["message"].ToString();
                    }
                }
                return Json(Json(resul).Data);
            } catch (Exception ex) {
                return Json(new cs_Result(1, ex.Message), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult questionSearch(Int32 answerId) {
            try {
                return Json(new cs_Result(0, new JavaScriptSerializer().Serialize(new cs_Question().questionAnswer(answerId)).ToString()));
            } catch (Exception ex) {
                return Json(new cs_Result(1, ex.Message));
            }
        }

        [HttpPost]
        public JsonResult RecoveryPassword(string email) {
            try {
                cs_Result result = new cs_Result();
                var searchEmail = db.Cat_Client.Where(a=> a.Email == email).ToList();
                if (searchEmail.Count > 0) {
                    
                    var newPass = new string("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789".OrderBy(o => Guid.NewGuid()).Take(8).ToArray());
                    var clt = db.Cat_Client.Where(s => s.Email == email).First();
                    clt.Password = SimpleEncryption.Encrypt(newPass, "FG");
                    db.SaveChanges(); // 

                    new cs_Email("noreply@kelloggs.com", "Bienvenido a Deseos Kellogg´s", email, "", "Deseos Kellogg´s", newPass, true, new ObjURL().getURLData(Request.Url.ToString())).sendMail();
                    return Json(new cs_Result(0, "Se ha enviado un Email con la nueva contraseña."));
                } else {
                    return Json(new cs_Result(1,"El email ingresado no existe, favor de validar la información."));
                }
            } catch (Exception ex) {
                return Json(new cs_Result(1, ex.Message));
            }
        }

        //*************** SHOW MODAL
        [HttpPost]
        public Object getState() {
            try {
                return ((from states in db.Cat_BranchOffice select new { states.State }).Distinct().OrderBy(a => a.State).ToList()).Select(x => x.State).ToList();
            } catch (Exception ex) {
                return null;
            }
        }

        //*************** SHOW MODAL
        [HttpPost]
        public JsonResult getLocation(string value) {
            try {
                return Json(Json(((from Location in db.Cat_BranchOffice
                                   where Location.State == value
                                   select new { Location.Location }).Distinct().OrderBy(a => a.Location).ToList()).Select(x => x.Location).ToList()).Data);
            } catch (Exception ex) {
                return null;
            }
        }

        //*************** SHOW MODAL
        [HttpPost]
        public JsonResult getSucursal(string value) {
            try {

                var data = (from suc in db.Cat_BranchOffice
                            where suc.Location == value
                            select new { SucursalNumberId = suc.SucursalNumberId, Name = suc.Name }).ToList();

                return Json(new cs_Result(0, new JavaScriptSerializer().Serialize(data).ToString()));

            } catch (Exception ex) {
                return null;
            }
        }

        //*************** SHOW MODAL
        [HttpPost]
        public void logOut() {
            Session.Clear();
            Session.RemoveAll();
            FormsAuthentication.SignOut();
        }

        /**************************************
         * CLOSE CONEXTION TO DATA BASE
         *************************************/
        protected override void Dispose(bool disposing) {
            if (disposing) {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}