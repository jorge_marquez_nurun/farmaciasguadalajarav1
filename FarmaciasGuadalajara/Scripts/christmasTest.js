﻿var initQuestion = 1;
$(function () {

    //console.clear();

    $("body>span").css("display", "none");

    $('#Question, .trivia').hide();

    $('.answer').click(function () {
        var value = $(this).attr('id');
        //console.log("answer-->" + value);
        $('#lblQuestioninit').remove();
        if (value == 'undefined' || value == undefined) {
            newModal('Por favor selecciona una respuesta.');
            return;
        }
        getQuestion(value);
        initQuestion = initQuestion + 1;
        console.log('initQuestion 1 => '+initQuestion);
    });

    $('#Total').keypress(function (e) {
        $(this).val($(this).val().replace(/[^0-9.]+/g, ''));
        isValidMoney($(this).val());
    });

    $('#SerialNumber').keypress(function (e) {
        $(this).val($(this).val().replace(/[^0-9]+/g, ''));
        isValidNumber($(this).val());
    });

    $('#State').change(function () {
        $.ajax({
            type: "POST",
            url: '/Main/getLocation',
            data: {
                value: $(this).val()
            },
            dataType: "json",
            success: function (data) {
                $('#Location,#SucursalNumber').empty().append('<option value="">-- Selecciona --</option>');
                $(data).each(function (index, elem) {
                    $('#Location').append('<option value="' + elem+'">' + elem+'</option>');
                });
            },
            statusCode: {
                404: function (content) { newModal('Error!', 'cannot find resource'); /*console.log(JSON.stringify(content));*/ },
                500: function (content) { newModal('Error!','internal server error'); /*console.log(JSON.stringify(content));*/ }
            },
            error: function (req, status, errorObj) {
                // handle status === "timeout"
                // handle other errors
            }
        });
    });

    $('#Location').change(function () {
        $.ajax({
            type: "POST",
            url: '/Main/getSucursal',
            data: {
                value: $(this).val()
            },
            dataType: "json",
            success: function (data) {
                $('#SucursalNumber').empty().append('<option value="">-- Selecciona --</option>');
                $($.parseJSON(data.message)).each(function (index, elem) {
                    $('#SucursalNumber').append('<option value="' + elem.SucursalNumberId + '">' + elem.Name + '</option>');
                });
            },
            statusCode: {
                404: function (content) { newModal('Error!', 'cannot find resource'); /*console.log(JSON.stringify(content));*/ },
                500: function (content) { newModal('Error!','internal server error'); /*console.log(JSON.stringify(content));*/ }
            },
            error: function (req, status, errorObj) {
                // handle status === "timeout"
                // handle other errors
            }
        });
    });

    if (!$('#Question #RbtAnswer').is('empty')) {
        $('#Question, .trivia').show();
    }
    if ($("#receipt2").is(":visible")) {
        $('#Question, .trivia').hide();
    }
    else {
        $('#nextStep').hide();
    }

    $('#nextStep').click(function () {
        if ($('#Receipt').is(':visible')) {
            validateData('#Receipt input,#Receipt select', registerticket);
        }
        /*else {
            if ($('#Question').is(':visible')) {
                var value = $('#Question #RbtAnswer input[name=answerId]:checked').val();
                console.log("answer-->" + value);
                if (value == 'undefined' || value == undefined) {
                    newModal('Por favor selecciona una respuesta.');
                    return;
                }
                getQuestion($('#Question #RbtAnswer input[name=answerId]:checked').val());

            }
        }*/
    });
});

function registerticket() {

    objElements['option'] = 'C';

    if (objElements.Total < 60) {
        newModal('El monto debe ser mayor o igual a $60.');
        return;
    }

    $.ajax({
        type: "POST",
        url: '/Main/ticketProcess', // + menuEvent,
        data: objElements,
        dataType: "json",
        success: function (data) {
            //console.log(data);
            if (data.code == 1) {
                var msj = data.message;
                //console.log(msj);
                if (data.message == "home") {
                    msj = "Tu sesión ha caducado, favor de ingresar tus credenciales nuevamente.";
                    setTimeout(function () {
                        window.location.href = '';
                    }, 3000);
                }
                else {
                    newModal('¡Hubo un error! ', msj);
                }
            } else {

                $('#nextStep').remove();
                $('#receipt2,#Receipt, #receipt3').hide();
                $('#Question, .trivia').show();

                var obj = $.parseJSON(data.message);

                $('#Question #lblQuestion').html('<label style="color:#f8e71c;"> 1 de 5</label></br>'+obj.questionName);
                $('#Question #RbtAnswer').empty();
                $.each(obj.data, function () {
                    $('#Question #RbtAnswer').append('<div class="answer" id="' + this.answerId + '" name="answerId" value="' + this.answerId + '">' + this.answerName + '</div>');
                });

                //initQuestion = initQuestion + 1;
                console.log('initQuestion 2 => ' + initQuestion);

                $('.answer').click(function () {
                    var value = $(this).attr('id');
                    //console.log("answer-->"+ value);
                    if (value == 'undefined' || value == undefined) {
                        newModal('Por favor selecciona una respuesta.');
                        return;
                    }
                    getQuestion(value);
                    initQuestion = initQuestion + 1;
                    console.log('initQuestion 3 => ' + initQuestion);
                });

            }
        },
        statusCode: {
            404: function (content) { newModal('Error!', 'cannot find resource'); /*console.log(JSON.stringify(content));*/ },
            500: function (content) { newModal('Error!','internal server error'); /*console.log(JSON.stringify(content));*/ }
        },
        error: function (req, status, errorObj) {
            // handle status === "timeout"
            // handle other errors
        }
    });

}

function getQuestion(answerId) {

    $.ajax({
        type: "POST",
        url: '/Main/questionSearch', // + menuEvent,
        data: {
            answerId: answerId
        },
        dataType: "json",
        success: function (data) {

            if (data.code == 1) {
                newModal('¡Hubo un error! ', data.message);
            } else {
                $('#receipt2,#Receipt, #receipt3').hide();
                $('#Question, .trivia').show();

                var obj = $.parseJSON(data.message);

                if (obj.questionId == 0) {
                    //Agregado Ariel:
                    $("#receipt2, #Receipt, .triviakill").off().remove();
                    $("#xmastest__ticket").addClass("tony").html('<div class="col-xs-10 col-sm-8 col-md-7 text-left white_on_white"><p class="fs-1_5 amarillo bold mb-0">Trivia resuelta</p><p>Recibimos tu participación. Podrás consultar la lista de ganadores a partir del <span class="amarillo">8 de enero de 2018.</span></p><p>Es muy importante que <span class="amarillo">conserves tu ticket de compra</span>, ya que si resultas ganador lo necesitarás para reclamar tu regalo.</p><p>&nbsp;</p></div>');
                    $('#xmastest__ticket').append('<button class="snowless" value="Terminar" id="nextStep">Siguiente</button>');
                    $('#nextStep').click(function () { window.location.href = '/Main/Index'; });
                    //fin de agregados Ariel
                    $('#nextStep').show();
                    //$('#Question #lblQuestion').html('¡LO CONSEGUISTE!<br>CONTESTASTE LA TRIVIA CORRECTAMENTE.');
                    //$('#Question #RbtAnswer').html('Pronto te notificaremos si eres ganador. Recuerda que no sólo cuenta<br>el haber resuelto las preguntas, sino el tiempo que te tomó hacerlo.</p>' +
                    //    'Es muy importante que conserves tu ticket de compra, ya que si<br>resultas premiado lo necesitarás para reclamar tu regalo.</p>');

                    //$('#Question #RbtAnswer').append('<input type="button" value="Terminar" id="nextStep" class="btn btn-primary" />');
                    //$('#nextStep').click(function () {
                    //    window.location.href = '/Main/Index';
                    //});
                }
                else {
                    var text = '';

                    switch (initQuestion) {
                        case 1:
                            text = '<label style="color:#f8e71c;"> 1 de 5</label></br>';
                            break;
                        case 2:
                            text = '<label style="color:#f8e71c;"> 2 de 5</label></br>';
                            break;
                        case 3:
                            text = '<label style="color:#f8e71c;"> 3 de 5</label></br>';
                            break;
                        case 4:
                            text = '<label style="color:#f8e71c;"> 4 de 5</label></br>';
                            break;
                        case 5:
                            text = '<label style="color:#f8e71c;"> 5 de 5</label></br>';
                            break;
                    }
                    
                    $('#Question #lblQuestion').html(text + obj.questionName);
                    $('#Question #RbtAnswer').empty();

                    $.each(obj.data, function () {
                        $('#Question #RbtAnswer').append('<div class="answer" id="' + this.answerId + '" name="answerId" value="' + this.answerId + '">' + this.answerName + '</div>');
                    });

                    $('.answer').click(function () {
                        var value = $(this).attr('id');
                       // console.log("answer-->" + value);
                        if (value == 'undefined' || value == undefined) {
                            newModal('Por favor selecciona una respuesta.');
                            return;
                        }
                        initQuestion = initQuestion + 1;
                        getQuestion(value);
                        console.log('initQuestion 4 => ' + initQuestion);
                    });

                }

            }
        },
        statusCode: {
            404: function (content) { newModal('Error!', 'cannot find resource'); /*console.log(JSON.stringify(content));*/ },
            500: function (content) { newModal('Error!','internal server error'); /*console.log(JSON.stringify(content));*/ }
        },
        error: function (req, status, errorObj) {
            // handle status === "timeout"
            // handle other errors
        }
    });
}