﻿var menuEvent = '';

$(function () {

    $('.logOut').click(function (e) {
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: '/Main/logOut', // + menuEvent,
            success: function (data) {
                window.location.href = '/Main/Index';
            },
            statusCode: {
                404: function (content) { newModal('Error!', 'cannot find resource'); /*console.log(JSON.stringify(content));*/ },
                500: function (content) { newModal('Error!', 'internal server error'); /*console.log(JSON.stringify(content));*/ }
            },
            error: function (req, status, errorObj) {
                // handle status === "timeout"
                // handle other errors
            }
        });
    });

    /* FUNCTION MENU INIT */
    //$('#Login,#Register').hide();
    
    
    $('#GoToStart').click(function () {
        $("#sidebar").removeClass("active");
        setTimeout(function () {
            window.location.href = "/Main/Index";
        }, 500);
    });

    $('#homeLogin, #Login').click(function() {
        menuEvent = "homeLogin";
        showPopData('/Main/popData', menuEvent, $(this).attr('data-title'), false, customerProcess, '', '');
        $("#sidebar").removeClass("active");
    });

    $('#Register').click(function () {
        $("#sidebar").removeClass("active");
        setTimeout(function () {
            window.location.href = "/Main/Register";
        }, 500);
    });

    $('#ChristmasTest').click(function () {
        if ($(this).attr('event') == 0) {
            //newModal('Error!', 'Debes iniciar sesión para poder participar.');
            menuEvent = "homeLogin";
            showPopData('/Main/popData', menuEvent, 'Iniciar sesión', false, customerProcess, '', '');
            return;
        }
        window.location.href = '/Main/ChristmasTest';
    });

    $('#profile, #MyAccount').click(function () {
        $("#sidebar").removeClass("active");
        setTimeout(function () {
            window.location.href = "/Main/profile";
        }, 500);
    });

    /*
    $("#NoticePrivacy").on("click", function () {
        showPrivacy();
    });

    $("#TermsConditions").on("click", function () {
        showTerms();
    });
    */

    //$('#Login,#Register,#TermsConditions,#NoticePrivacy').click(function () {
    //    //$('#Login,#Register').hide();
    //    menuEvent = $(this).attr('id');
    //    showPopData('/Main/popData', menuEvent, $(this).attr('title'), true, customerProcess, '');
    //    //showPopData($(this).attr('title'), customerProcess);

    //    $("#sidebar").removeClass("active");
    //});

    $('#RecoveryPassword').click(function() {
        menuEvent = $(this).attr('id');
        showPopData('/Main/popData', menuEvent, $(this).attr('title'), true, RecoveryPassword, '');
        //showPopData($(this).attr('title'), RecoveryPassword);
    });

    /* FUNCTION MENU END */

    /* SIDEBAR MENU */
    $("#menu_mobile").on("click", function() {
        $("#sidebar").toggleClass("active");
    });
    $("#sidebar_close").on("click", function() {
        $("#sidebar").removeClass("active");
    });

    //Para la pantalla Home
    if ($("#home").length > 0) {
        $(".fade_prize").hide();

        $("#home__btnpremio").on("click", function() {
            $(".fade_home").hide();
            $(".fade_prize").show();
        });
        $("#home__btnhome").on("click", function() {
            $(".fade_prize").hide();
            $(".fade_home").show();
        });
    }


    MakeItSnow();
    //$(window).resize(function () {
    //    $(".snow").remove();
    //    MakeItSnow();
    //});

});

//function register() {
//    menuEvent = 'Register';
//    showPopData('/Main/popData', 'Register', 'Crear Cuenta', true, customerProcess, '', '');
//}

function recoveryPassword() {
    menuEvent = 'RecoveryPassword';
    showPopData('/Main/popData', 'RecoveryPassword', 'Recuperar Contraseña', true, customerProcess, '', '');
}

function showTerms() {
    showPopData('/Main/popData', "TermsConditions", "Términos y Condiciones", false, "", '', '');
    $("#sidebar").removeClass("active");
}

function showPrivacy() {
    showPopData('/Main/popData', "NoticePrivacy", "Aviso de Privacidad", false, "", '', '');
    $("#sidebar").removeClass("active");
}


function customerProcess() {

    //console.log('opt => '+menuEvent);

    switch (menuEvent) {
        case "homeLogin":
            objElements['option'] = 'R';
            if (escapeHtml(objElements['Password']) != objElements['Password']) {
                newModal('¡Error!', '<p>La contraseña debe tener al entre 8 y 16 caracteres</p> <p>Al menos un dígito, al menos una minúscula y al menos una mayúscula.</p> <p>Símbolos permitidos <b>"$@!%*._"</b>.</p><p>Ejemplo: <b>M1c0ntr@señ4.01</b></p>');
                return;
            }
            if (!validatePass(escapeHtml(objElements['Password']))) {
                newModal('¡Error!', '<p>La contraseña debe tener al entre 8 y 16 caracteres</p> <p>Al menos un dígito, al menos una minúscula y al menos una mayúscula.</p> <p>Símbolos permitidos <b>"$@!%*._"</b>.</p><p>Ejemplo: <b>M1c0ntr@señ4.01</b></p>');
                return;
            }
            break;
        case "RecoveryPassword":
            objElements['option'] = 'RE';
            break;
        case "Register":
            objElements['option'] = 'C';
            if (escapeHtml(objElements['Password']) != objElements['Password']) {
                newModal('¡Error!', '<p>La contraseña debe tener al entre 8 y 16 caracteres</p> <p>Al menos un dígito, al menos una minúscula y al menos una mayúscula.</p> <p>Símbolos permitidos <b>"$@!%*._"</b>.</p><p>Ejemplo: <b>M1c0ntr@señ4.01</b></p>');
                return;
            }
            if (!validatePass(escapeHtml(objElements['Password']))) {
                newModal('¡Error!', '<p>La contraseña debe tener al entre 8 y 16 caracteres</p> <p>Al menos un dígito, al menos una minúscula y al menos una mayúscula.</p> <p>Símbolos permitidos <b>"$@!%*._"</b>.</p><p>Ejemplo: <b>M1c0ntr@señ4.01</b></p>');
                return;
            }
            break;
    }
    
    $.ajax({
        type: "POST",
        url: '/Main/customerProcess', // + menuEvent,
        data: objElements,
        dataType: "json",
        success: function(data) {
            if (data.code == 1) {
                //console.log(data.message);
                //console.log($.parseJSON(data.message));
                if (menuEvent == 'Register') {
                    grecaptcha.reset(); // CAPTCHA REACTIVATE
                }
                newModal('¡Hubo un error!', $.parseJSON(data.message).message);
            } else {
                switch (menuEvent) {
                    case "homeLogin":
                        newModal('¡Bien hecho!', 'Bienvenido, ' + data.message);
                        setTimeout(function() {
                            location.reload();
                        }, 3000);
                        break;
                    case "RecoveryPassword":
                        newModal('¡Bien hecho!', $.parseJSON(data.message).message);
                        setTimeout(function () {
                            location.reload();
                        }, 3000);                        
                        break;
                    case "Register":
                        newModal('¡Bien hecho!', 'Bienvenido, ' + data.message);
                        setTimeout(function() {
                            window.location.href = "/Main/Gracias";
                        }, 3000);
                        break;
                }
            }
        },
        statusCode: {
            404: function (content) { newModal('Error!', 'cannot find resource'); /*console.log(JSON.stringify(content));*/ },
            500: function (content) { newModal('Error!', 'internal server error'); /*console.log(JSON.stringify(content));*/ }
        },
        error: function(req, status, errorObj) {
            // handle status === "timeout"
            // handle other errors
        }
    });

}

function RecoveryPassword(email) {

    $.ajax({
        type: "POST",
        url: '/Main/RecoveryPassword', // + menuEvent,
        data: {
            email: $('#Email').val()
        },
        dataType: "json",
        success: function(data) {
            var title = '';
            if (data.code == 1) {
                title = 'Error!';
            } else {
                title = 'Success!';
                setTimeout(function() {
                    $('#myModal').modal('hide');
                }, 3000);
            }

            newModal(title, data.message);

        },
        statusCode: {
            404: function (content) { newModal('Error!', 'cannot find resource'); /*console.log(JSON.stringify(content));*/ },
            500: function (content) { newModal('Error!', 'internal server error'); /*console.log(JSON.stringify(content));*/ }
        },
        error: function(req, status, errorObj) {
            // handle status === "timeout"
            // handle other errors
        }
    });

}

