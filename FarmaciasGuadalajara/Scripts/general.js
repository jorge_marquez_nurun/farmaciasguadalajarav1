﻿var objElements = {};
var arrayElementEmpty = [];
var GAN = '';
var SGAN = '';

$(function () { });

function validateField(data) {

    $(data + ' .money').mask('999.999.999-99', { reverse: true });
    //$(data + ' .number').mask('99999999');
    $(data + ' .phone').mask('(99)-9999-9999');
    $(data + ' .celular').mask('(999)-99-9999-9999');

    $(data + ' .email').change(function () {
        $(this).removeClass('error');
        if (!isValidEmailAddress($(this).val())) {
            $(this).addClass('error');
            newModal('Error!', 'Formato invalido para el correo.');
        }
    });

}

function showPopData(url, option, title, validate, callback, value, callback2) {
    //console.log("title-->" + title);
    $.ajax({
        type: "POST",
        url: url, //'/Main/popData',
        traditional: true,
        data: {
            option: option
        },
        success: function (data) {
            ////console.clear();
            //console.log("data->"+data);
            $('#myModal .modal-title').html(title);
            $('#myModal').modal();
            $('#myModal .modal-body,#myModal .modal-footer').remove();
            $('#myModal .modal-content').append(data);
            $('#btnOk').click(function () {
                validateData('#myModal .modal-body input', callback);
            });

            if (validate) {
                validateField('#myModal .modal-content');
            }

            if (callback2 != '') {
                if (value != '') {
                    callback2(value);
                }
                else {
                    callback2();
                }
            }

        },
        statusCode: {
            404: function (content) { newModal('Error!', 'cannot find resource'); /*console.log(JSON.stringify(content));*/ },
            500: function (content) { newModal('Error!','internal server error'); /*console.log(JSON.stringify(content));*/ }
        },
        error: function (req, status, errorObj) {
            // handle status === "timeout"
            // handle other errors
        }
    });

}

function validateData(elements, callback, typemessage) {

    objElements = {};
    arrayElementEmpty = [];
    
    $(elements).each(function () {
        
        if (this.type == 'checkbox') {
            //console.log($(this).attr('id') + ' : ' + $(this).prop('checked'));
            objElements[$(this).attr('id')] = $(this).prop('checked');
        }
        else {
            //console.log($(this).attr('id') + ' : ' + $(this).val());
            objElements[$(this).attr('id')] = $(this).val();
        }

        if ($.trim($(this).val()) == '') {
            $(this).addClass('error');
            switch (this.type) {
                case 'checkbox':
                    if (!$(this).prop('checked')) {
                        //arrayElementEmpty.push($(this).parent().find('label').text().replace(':', '') + '"');
                        var valor = $(this).parent().find('label').text().replace(':', '');
                        if (valor == '') {
                            valor = $(this).attr('placeholder');
                        }
                        arrayElementEmpty.push(valor);
                    }
                    break;
                default:
                    //arrayElementEmpty.push($(this).parent().find('label').text().replace(':', ''));
                    var valor = $(this).parent().find('label').text().replace(':', '');
                    if (valor == '') {
                        valor = $(this).attr('placeholder');
                    }
                    arrayElementEmpty.push(valor);
                    break;
            }
        } else {
            $(this).removeClass('error');
            switch (this.type) {
                case 'email':
                    if (!isValidEmailAddress($(this).val())) {
                        arrayElementEmpty.push('Formato invalido para el campo "Email"');
                    }
                    break;
                case 'number':
                    if (!isValidNumber($(this).val().replace('.', ''))) {
                        arrayElementEmpty.push('Formato invalido para el campo un campo númerico"');
                    }
                    break;
                case 'checkbox':
                    objElements[$(this).attr('id')] = 1;
                    if (!$(this).prop('checked')) {
                        var valor = $(this).parent().find('label').text().replace(':', '');
                        if (valor == '') {
                            valor = $(this).attr('placeholder');
                        }
                        arrayElementEmpty.push(valor);
                    }
                    break;
            }
        }

    })
    .promise()
    .done(function () {
        //console.log(arrayElementEmpty);
        if (!$.isEmptyObject(arrayElementEmpty)) {
            switch (typemessage) {
                case 0:
                    newModal('¡Error!','<div class="modal-body">Favor de validar la información...</div>');
                    break;
                case 1:
                    newModal('¡Error!','<div class="modal-body">Existen campos vacios...</div > ');
                    break;
                case 2:
                    newModal('¡Error!','Favor de validar los siguientes campos.', arrayElementEmpty.join('</div>, <div>'));
                    break;
            }
            return;
        }
        callback();
    });
}

function newModal(title, data) {
    //console.log(data);
    var id = "newModal"+Math.floor((Math.random()) * (1000 - 1)) + 1;
    var newModal = $('#myModal').clone(false, true);

    newModal.attr('id', id);
    $('body').append(newModal);
    $('#' + id + ' .modal-header,#' + id +' .modal-footer').remove();
    $('#' + id +' .modal-body').html(title);
    $('#' + id +' .modal-content').html(data);
    $('#'+id).modal('show');

    setTimeout(function () {
        $('#' + id).modal('hide');
        //$(".modal-backdrop").delay(1000).fadeOut(200, function () { $(".modal-backdrop").remove(); });
    }, 3000);
}

function isValidEmailAddress(value) {
    var pattern = new RegExp(/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/);
    return pattern.test(value);
};

function isValidNumber(value) {
    var pattern = new RegExp(/\(?([0-9])/);
    return pattern.test(value);
};

function isValidMoney(value) {
    var pattern = new RegExp(/(?=.)^\$?(([1-9][0-9]{0,2}(,[0-9]{3})*)|0)?(\.[0-9]{1,2})?$/);
    return pattern.test(value);
}

function validatePass(value) {
    var pattern = new RegExp(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*._])([A-Za-z\d$@$!%*._]|[^]){8,16}$/);
    return pattern.test(value);
}

var entityMap = {
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;',
    '"': '&quot;',
    "'": '&#39;',
    '/': '&#x2F;',
    '\\': '&#92;',
    '`': '&#x60;',
    '+': '&#43;',
    '?': '&#63;',
    '=': '&#x3D;'
};

function escapeHtml(string) {
    return String(string).replace(/[&<>"'`=\/+?\\]/g, function (s) {
        return entityMap[s];
    });
}