﻿var optionClick = '';
$(function() {
    ////console.clear();
});

function getClientList(show,page) {
    $.ajax({
        type: "POST",
        url: '/CMS/getClientListWinners',
        traditional: true,
        dataType: "json",
        data: {
            RowsPerPage: show,
            PageNumber: page
        },
        success: function (data) {
            //console.log(data);
            if (data.code == 1) {
                newModal('Error!', data.message);
            } else {
                $('#tblclientList tbody').empty();
                var res = $.parseJSON(data.message);
                //$(res).each(function (index, element) {
                //console.log(res);
                $(res).each(function (index, element) {
                    //console.log(index);
                    //console.log(element['ClientId'] + ' | ' + element['Name'] + ' | ' + element['Email'] + ' | ' + element['LadaCelular'] + ' | ' + element['Phone']);
                    $('#tblclientList tbody').append('<tr>' +
                        '<td>' + element['Lugar'] + '</td>' +
                        '<td>' + element['Nombre'] + '</td>' +
                        '<td>' + element['Aciertos'] + '</td>' +
                        '<td>' + element['Tiempo'] + '</td>' +
                    '</tr>');
                });
            }
        },
        statusCode: {
            //404: function (content) { newModal('Error!', 'cannot find resource'); /*console.log(JSON.stringify(content));*/ },
            //500: function (content) { newModal('Error!','internal server error'); /*console.log(JSON.stringify(content));*/ }
        },
        error: function (req, status, errorObj) {
            // handle status === "timeout"
            // handle other errors
        }
    });
}