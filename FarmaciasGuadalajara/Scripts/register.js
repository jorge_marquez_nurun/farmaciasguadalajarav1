﻿$(function() {

    //console.clear();
   
    validateField('#register');

    $('#Name,#LastName1,#LastName2').keypress(function () {
        $(this).val($(this).val().replace(/[^a-zA-Z ]/g, ''));
    }).change(function () {
        $(this).val($(this).val().replace(/[^a-zA-Z ]/g, ''));
    });

    $('#LadaPhone').keypress(function () {
        $(this).val($(this).val().slice(0, 3));
    }).change(function () {
        $(this).val($(this).val().slice(0, 3));
    });

    $('#Phone').keypress(function () {
        $(this).val($(this).val().slice(0, 10));
    }).change(function () {
        $(this).val($(this).val().slice(0, 10));
    });

    $('#LadaPhone,#Phone').keypress(function(e) {
        $(this).val($(this).val().replace(/[^0-9.]+/g, ''));
        isValidMoney($(this).val());
    }).change(function (e) {
        $(this).val($(this).val().replace(/[^0-9.]+/g, ''));
        isValidMoney($(this).val());
    });

    $('#btnOk').click(function() {

        if ($('#Password').val() != $('#PasswordChk').val()) {
            newModal('<div class="modal-body">Las contraseñas no coinciden...</div>');
            return;
        }

        if (SGAN == 1) {
            validateData('#register input', validaCaptcha); // CAPTCHA REACTIVATE
        }
        else {
            menuEvent = 'Register';
            objElements['option'] = 'Register';
            validateData('#register input', customerProcess); // CAPTCHA REACTIVATE
        }

        /*** quitar para CAPTCHA REACTIVATE ****/
        //menuEvent = 'Register';
        //objElements['option'] = 'Register';
        //validateData('#register input', customerProcess);        
        /*** quitar ****/

    });

});

function validaCaptcha() {
    var gcData = new FormData();
    var gc = grecaptcha.getResponse(); gcData.append("g-recaptcha-response", gc); // CAPTCHA REACTIVATE

    $.ajax({
        type: "POST",
        url: "/Main/ManageReCaptcha",
        processData: false,
        contentType: false,
        async: false,
        data: gcData,
        success: function(data) {
            var data = jQuery.parseJSON(data);
            if (!data["success"]) { newModal("Debes completar el captcha para poder registrarte."); grecaptcha.reset(); return; } // CAPTCHA REACTIVATE
            menuEvent = 'Register';
            objElements['option'] = 'Register';
            customerProcess();
        },
        statusCode: {
            404: function (content) { newModal('Error!', 'cannot find resource'); /*console.log(JSON.stringify(content));*/ },
            500: function (content) { newModal('Error!','internal server error');  /*console.log(JSON.stringify(content));*/ }
        },
        error: function(req, status, errorObj) {
            // handle status === "timeout"
            // handle other errors
        }
    });
}