﻿var optionClick = '';
$(function() {
    ////console.clear();
});

function customerProcess() {

    objElements['option'] = option;

    $.ajax({
        type: "POST",
        url: '/CMS/customerProcess',
        data: objElements,
        dataType: "json",
        success: function (data) {;
            if (data.code == 1) {
                newModal('Error!', data.message);
            } else {
                var obj = $.parseJSON(data.message);

                //console.log(obj);

                if (obj.message == null) {
                    if (optionClick == 'showClient') {
                        $('#myModal .modal-content #FullName').html(obj.Name + ' ' + obj.LastName1 + ' ' + obj.LastName2);
                        $('#myModal .modal-content #Email').html(obj.Email);
                        $('#myModal .modal-content #Phone').html(obj.Phone);
                        $('#myModal .modal-content #Celular').html(obj.Celular);
                    }
                    else {
                        $.each(obj, function(name, value) {
                            $('#myModal .modal-content #' + name).val(value);
                        });
                        validateField('#register');

                        $('#LadaPhone').keypress(function () {
                            $(this).val($(this).val().slice(0, 3));
                        });

                        $('#Phone').keypress(function () {
                            $(this).val($(this).val().slice(0, 10));
                        });

                        $('#LadaPhone,#Phone').keypress(function (e) {
                            $(this).val($(this).val().replace(/[^0-9.]+/g, ''));
                            isValidMoney($(this).val());
                        });
                        option = 'U';
                    }
                }
                else {
                    newModal('Success!', obj.message);
                    $('#myModal').modal('hide');
                    getClientList(1);
                }
            }
        },
        statusCode: {
            404: function (content) { newModal('Error!', 'cannot find resource'); /*console.log(JSON.stringify(content));*/ },
            500: function (content) { newModal('Error!','internal server error'); /*console.log(JSON.stringify(content));*/ }
        },
        error: function (req, status, errorObj) {
            // handle status === "timeout"
            // handle other errors
        }
    });
}

function clientTicketResultList(valor) {

    //console.log(valor);

    $.ajax({
        type: "POST",
        url: '/CMS/clientTicketResultList',
        traditional: true,
        dataType: "json",
        data: {
            ClienteId: valor
        },
        success: function (data) {
            if (data.code == 1) {
                newModal('Error!', data.message);
            } else {
                option = 'Tickets';
                showPopData('/CMS/popData', option, 'Tickets', false, '', $.parseJSON(data.message), showResultTicket);
            }
        },
        statusCode: {
            //404: function (content) { newModal('Error!', 'cannot find resource'); /*console.log(JSON.stringify(content));*/ },
            //500: function (content) { newModal('Error!','internal server error'); /*console.log(JSON.stringify(content));*/ }
        },
        error: function (req, status, errorObj) {
            // handle status === "timeout"
            // handle other errors
        }
    });
}

function showResultTicket(object) {

    //console.clear();
    //console.log(object);

    var Total = 0;
    var TotalRegistros = 0;

    $(object).each(function (index, element) {
        Total = Total + element.Monto;
        TotalRegistros = TotalRegistros + 1;
        $('#containerData #tiketContainerResult').append('<div class="panel-heading">' +
                '<h4 class="panel-title">' +
                    '<a data-toggle="collapse" data-parent="#accordion" href="#' + index + '">' +
                        '<span class="glyphicon glyphicon-folder-close">' + '</span> Número de Serie : ' + element.Folio +
                    '</a>' +
                '</h4>' +
            '</div>' +
            '<div id="' + index + '" class="panel-collapse collapse">' +
                '<div class="panel-body">' +
                    //'Número de Folio: <b>' + element.Folio + '</b></br>' +
                    'Fecha de Registro: <b>' + element.FechaRegistro + '</b></br>' +
                    'Inicio de Encuesta: <b>' + element.InicioEncuesta + '</b></br>' +
                    'Sucursal: <b>' + element.Sucursal + '</b></br>' +
                    'Monto: <b>' + element.Monto + '</b></br>' +
                    'Asiertos: <b>' + element.Asiertos + '</b></br>' +
                    '<table class="table" id="ListQuestions' + index + '"></table>' +
                '</div>' +
            '</div>')
            .promise()
            .done(function () {
                //console.log('--Tikets => ' + index+'--');
                //console.log(object[index]);
                $(object[index].Preguntas).each(function (key, value) {
                    //console.log('--- Questions => ' + index + '_' + key+ '-----');
                    //console.log(object[index].Preguntas[key]);
                    $('#ListQuestions' + index).append('<tr>' +
                        '<td><p>' + object[index].Preguntas[key].Nombre +
                        '<div id="tdAnswers' + index + '_' + key + '"></div>' +
                        '</p></td>' +
                        '</tr>')
                        .promise()
                        .done(function () {
                            $(object[index].Preguntas[key].Respuestas).each(function (row, val) {
                                //console.log(object[index].Preguntas[key].Respuestas[row]);
                                var correct = '';

                                if (object[index].Preguntas[key].Respuestas[row].RespuestaSeleccionada == object[index].Preguntas[key].Respuestas[row].RespuestaId) {
                                    correct = '<span class="glyphicon glyphicon-record" aria-hidden="true"></span>';
                                }

                                var text = '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span> ' + object[index].Preguntas[key].Respuestas[row].Respuesta + ' ' + correct + '<br>';
                                if (object[index].Preguntas[key].Respuestas[row].Correcta == 0) {
                                    text = '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>' + object[index].Preguntas[key].Respuestas[row].Respuesta + ' ' + correct + '<br>';
                                }


                                $('#tdAnswers' + index + '_' + key).append(' ' + text + '<br>');
                            });

                            //console.log('--- Answers-----');
                        });
                });
                //console.log('---------------' + index + '---------------');

            });
    }).promise().done(function () { $('#myModal .modal-title').html('Tickets Registrados: ' + TotalRegistros + '</br>Monto Total: ' + Total); });

}

function getClientList(page) {
    //console.log(1);
    $.ajax({
        type: "POST",
        url: '/CMS/getClientList',
        traditional: true,
        dataType: "json",
        data: {
            RowsPerPage: 10,
            PageNumber: page
        },
        success: function (data) {
            //console.log(data);
            if (data.code == 1) {
                newModal('Error!', data.message);
            } else {
                $('#tblclientList tbody').empty();
                var res = $.parseJSON(data.message);
                var Page = 1;
                //$(res).each(function (index, element) {
                //console.log(res);
                $(res).each(function (index, element) {
                    //console.log(index);
                    //console.log(element['ClientId'] + ' | ' + element['Name'] + ' | ' + element['Email'] + ' | ' + element['LadaCelular'] + ' | ' + element['Phone']);
                    $('#tblclientList tbody').append('<tr>' +
                        '<td>' + element['Name'] + '</td>' +
                        '<td>' + element['Email'] + '</td>' +
                        '<td>(' + element['LadaCelular'] + ') ' + element['Phone'] + '</td>' +
                        '<td>' + element['Date'].substring(0, 10) + '</td>' +
                        '<td>'+
                        '<div onclick="clientTicketResultList(' + element['ClientId'] + ')"><span class="glyphicon glyphicon-search cursor" aria-hidden="true"></span></div>' +
                        '</td>' +
                        '<td>' +
                        '<span class="glyphicon glyphicon-pencil cursor" val="' + element['ClientId'] + '" id="spn_EditClient"></span> &emsp; | &emsp; <span class="glyphicon glyphicon-search cursor" val="' + element['ClientId'] + '" id="spn_showClient" aria-hidden="true"></span' +
                        '</td>' +
                        '</tr>');

                    Page = parseInt(element['Status']);

                }).promise()
                    .done(function () {                        
                        $('span[id=spn_EditClient]').click(function () {
                            clid = $(this).attr('val');
                            option = 'R';
                            optionClick = 'EditClient';
                            objElements['ClientId'] = clid;
                            showPopData('/CMS/popData', 'EditClient', 'Editar Cliente', true, customerProcess, '', customerProcess);
                        });

                        $('span[id=spn_showClient]').click(function () {
                            clid = $(this).attr('val');
                            option = 'R';
                            optionClick = 'showClient';
                            objElements['ClientId'] = clid;
                            showPopData('/CMS/popData', 'ShowClient', 'Datos Cliente', false, '', '', customerProcess);
                        });

                        $('#LadaPhone').keypress(function () {
                            $(this).val($(this).val().slice(0, 2));
                        });

                        $('#Phone').keypress(function () {
                            $(this).val($(this).val().slice(0, 9));
                        });

                        $('#LadaPhone,#Phone').keypress(function (e) {
                            $(this).val($(this).val().replace(/[^0-9.]+/g, ''));
                            isValidMoney($(this).val());
                        });

                        $('#pagination').twbsPagination({
                            totalPages: Page,
                            visiblePages: 10,
                            onPageClick: function (event, page) {
                                getClientList(page)
                                //console.info(event);
                            }
                        });


                    });
            }
        },
        statusCode: {
            //404: function (content) { newModal('Error!', 'cannot find resource'); /*console.log(JSON.stringify(content));*/ },
            //500: function (content) { newModal('Error!','internal server error'); /*console.log(JSON.stringify(content));*/ }
        },
        error: function (req, status, errorObj) {
            // handle status === "timeout"
            // handle other errors
        }
    });
}