﻿var body = document.getElementsByClassName("bgimg")[0];
    var snowholder = document.createElement("div");
    var canvas_ = document.createElement("canvas");
    canvas_.setAttribute("id", "canvas");
    body.prepend(snowholder);
    snowholder.className = "snowholder";
    snowholder.append(canvas_);

function MakeItSnow() {
    var canvas = document.getElementById('canvas'),
        ctx = canvas.getContext('2d'),
        w = canvas.width = window.innerWidth,
        h = canvas.height = window.innerHeight,
        density = 1,
        particles = [],
        particlesToRemove = [],
        options = {
            particles: 100,
            speed: 0.4, // mess with this
            maxSize: 2,
            hue: 190,
            brightness: 60
        };

    function setup() {
        window.addEventListener('resize', resize);

        density = window.devicePixelRatio != undefined ? window.devicePixelRatio : 1.0;

        canvas.width = w * density;
        canvas.height = h * density;

        ctx.scale(density, density);
        ctx.fillStyle = 'hsl(' + options.hue + ', 50%, ' + options.brightness + '%)';
        ctx.shadowColor = 'hsl(' + options.hue + ', 20%, ' + options.brightness + '%)';

        pushParticles();
        draw();
    }

    function pushParticles() {
        for (var i = 0; i < options.particles; i++) {
            pushParticle();
        }
    }

    function pushParticle(fromTop) {
        var part = new Particle(Math.random() * w, Math.random() * h);
        part.xSpeed = (options.speed * 3) - (Math.random() * (options.speed * 6));
        part.ySpeed = options.speed;
        part.size = Math.random() * options.maxSize;

        if (fromTop) {
            part.y = -(part.size * 2);
        }

        particles.push(part);
    }

    function pushParticlesIfNeeded() {
        var pushNumber = options.particles - particles.length;

        if (pushNumber > 0) {
            for (var i = 0; i < pushNumber; i++) {
                pushParticle(true);
            }
        }
    }

    function draw() {
        ctx.clearRect(0, 0, w, h);

        ctx.shadowBlur = 5;
        ctx.globalCompositeOperation = "lighter";

        particles.forEach(function (part) {
            part.draw();
        });

        particlesToRemove.forEach(function (part) {
            part.destroy();
        });

        particlesToRemove = [];

        pushParticlesIfNeeded();

        window.requestAnimationFrame(draw);
    }

    function resize() {
        w = canvas.width = window.innerWidth;
        h = canvas.height = window.innerHeight;

        canvas.width = w * density;
        canvas.height = h * density;

        ctx.scale(density, density);

        particles = [];
        pushParticles();
    }

    var Particle = function (x, y) {
        this.x = x;
        this.y = y;
        this.xSpeed = 0;
        this.ySpeed = 0;
        this.size = 0;

        var self = this;

        this.draw = function () {
            ctx.beginPath();

            self.x += self.xSpeed;
            self.y += self.ySpeed;

            ctx.arc(self.x, self.y, self.size, 0, 2 * Math.PI);
            ctx.fillStyle = 'hsl(' + options.hue + ', 50%, ' + options.brightness + '%)';
            ctx.shadowColor = 'hsl(' + options.hue + ', 20%, ' + options.brightness + '%)';
            ctx.fill();

            ctx.closePath();

            if ((self.x > (w + (self.size * 2))) || (self.x < -(self.size * 2))) {
                particlesToRemove.push(self);
            }

            if (self && self.y > h + (self.size * 2)) {
                particlesToRemove.push(self);
            }
        };

        this.destroy = function () {
            var index = particles.indexOf(self);

            if (index > -1) {
                particles.splice(index, 1);
            }

            self = null;
        };
    };

    setup();




}