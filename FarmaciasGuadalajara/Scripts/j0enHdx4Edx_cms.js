﻿var option = '';
var controller = '';
var clid = 0;

$(function () {

    ////console.clear();

    $('[data-toggle="control-sidebar"]').controlSidebar()
    $('[data-toggle="push-menu"]').pushMenu()

    var $pushMenu = $('[data-toggle="push-menu"]').data('lte.pushmenu')
    var $controlSidebar = $('[data-toggle="control-sidebar"]').data('lte.controlsidebar')
    var $layout = $('body').data('lte.layout')


    $('#logout').click(function () {
        $.ajax({
            type: "POST",
            url: '/CMS/logOut', // + menuEvent,
            success: function (data) {
                window.location.href = '/CMS/j0enHdx4Edx';
            },
            statusCode: {
                404: function (content) { newModal('Error!', 'cannot find resource'); /*console.log(JSON.stringify(content));*/ },
                500: function (content) { newModal('Error!', 'internal server error'); /*console.log(JSON.stringify(content));*/ }
            },
            error: function (req, status, errorObj) {
                // handle status === "timeout"
                // handle other errors
            }
        });
    });

    $('#btnOk').click(function () {
        option = 'R';
        controller = 'adminProcess';
        validateData('.login-box-body input', adminProcess);
    });
    
});

function adminProcess() {

    objElements['option'] = option;

    if (escapeHtml(objElements['Password']) != objElements['Password']) {
        newModal('¡Error!', '<p>La contraseña debe tener al entre 8 y 16 caracteres</p> <p>Al menos un dígito, al menos una minúscula y al menos una mayúscula.</p> <p>Símbolos permitidos <b>"$@!%*._"</b>.</p><p>Ejemplo: <b>M1c0ntr@señ4.01</b></p>');
        return;
    }
    if (!validatePass(escapeHtml(objElements['Password']))) {
        newModal('¡Error!', '<p>La contraseña debe tener al entre 8 y 16 caracteres</p> <p>Al menos un dígito, al menos una minúscula y al menos una mayúscula.</p> <p>Símbolos permitidos <b>"$@!%*._"</b>.</p><p>Ejemplo: <b>M1c0ntr@señ4.01</b></p>');
        return;
    }

    $.ajax({
        type: "POST",
        url: '/CMS/adminProcess',
        data: objElements,
        dataType: "json",
        success: function (data) {

            //console.log(data);

            if (data.code == 1) {
                newModal('Error!', $.parseJSON(data.message).message);
            } else {
                newModal('Success!', 'Bienvenido ' + data.message);
                setTimeout(function () {
                    window.location.href = "/CMS/dashBoard";
                }, 3000);
            }
        },
        statusCode: {
            404: function (content) { newModal('Error!', 'cannot find resource'); /*console.log(JSON.stringify(content));*/ },
            500: function (content) { newModal('Error!','internal server error'); /*console.log(JSON.stringify(content));*/ }
        },
        error: function (req, status, errorObj) {
            // handle status === "timeout"
            // handle other errors
        }
    });
}

//function showPopData(title, callback, object) {
//    //console.log(object);
//    $.ajax({
//        type: "POST",
//        url: '/CMS/popData',
//        traditional: true,
//        data: {
//            option: option
//        },
//        success: function (data) {
//            ////console.clear();
//            $('#myModal .modal-title').html(title);
//            $('#myModal').modal();
//            $('#myModal .modal-body,#myModal .modal-footer').remove();
//            $('#myModal .modal-content').append(data);
//            callback(object);
//        },
//        statusCode: {
//            //404: function (content) { newModal('Error!', 'cannot find resource'); /*console.log(JSON.stringify(content));*/ },
//            //500: function (content) { newModal('Error!','internal server error'); /*console.log(JSON.stringify(content));*/ }
//        },
//        error: function (req, status, errorObj) {
//            // handle status === "timeout"
//            // handle other errors
//        }
//    });

//}