//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FarmaciasGuadalajara.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Tbl_Transaction
    {
        public long TransactionId { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public Nullable<long> ClientId { get; set; }
        public Nullable<long> TicketId { get; set; }
        public Nullable<int> QuestionId { get; set; }
        public Nullable<int> AnswerId { get; set; }
        public Nullable<System.DateTime> Time { get; set; }
        public Nullable<int> AdminId { get; set; }
        public string Comentary { get; set; }
        public Nullable<int> Status { get; set; }
    
        public virtual Cat_Admin Cat_Admin { get; set; }
        public virtual Cat_Answer Cat_Answer { get; set; }
        public virtual Cat_Client Cat_Client { get; set; }
        public virtual Tbl_Ticket Tbl_Ticket { get; set; }
    }
}
