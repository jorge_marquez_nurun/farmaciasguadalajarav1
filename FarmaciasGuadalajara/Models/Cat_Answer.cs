//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FarmaciasGuadalajara.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Cat_Answer
    {
        public Cat_Answer()
        {
            this.Tbl_Transaction = new HashSet<Tbl_Transaction>();
        }
    
        public int AnswerId { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public string Name { get; set; }
        public Nullable<int> QuestionId { get; set; }
        public Nullable<int> Correct { get; set; }
        public Nullable<int> Status { get; set; }
    
        public virtual ICollection<Tbl_Transaction> Tbl_Transaction { get; set; }
    }
}
