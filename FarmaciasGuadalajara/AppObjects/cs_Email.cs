﻿   using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;

namespace FarmaciasGuadalajara.AppObjects {
    public class cs_Email {

        private string from { get; set; }
        private string subject { get; set; }
        private string to { get; set; }
        private string copy { get; set; }
        private string title { get; set; }
        private string body { get; set; }
        private bool html { get; set; }
        private cs_ObjURLImage url { get; set; }
        private string beforTable { get; set; }

        public cs_Email() {
        }

        public cs_Email(string from, string subject, string to, string copy, string title, string body, bool html = true, cs_ObjURLImage url = null, string beforTable = "") {
            this.from = from;
            this.subject = subject;
            this.to = to;
            this.copy = copy;
            this.title = title;
            this.body = body;
            this.html = html;
            this.url = url;
            this.beforTable = beforTable;
        }

        public string sendMail() {
            MailMessage mail = new MailMessage();
            SmtpClient SmtpServer = new SmtpClient();

            try {
                mail.From = new MailAddress(this.from, this.subject, Encoding.UTF8);
                mail.To.Add(this.to);
                if (this.copy != "") {
                    mail.To.Add(this.copy);
                }
                mail.Subject = this.title;

                if (html) {
                    mail.Body = tableEmail(this.body, url, this.beforTable);
                } else {
                    mail.Body = body;
                }

                mail.Priority = System.Net.Mail.MailPriority.Normal;
                mail.IsBodyHtml = html;
                mail.BodyEncoding = System.Text.Encoding.UTF8;
                SmtpServer.Credentials = new System.Net.NetworkCredential("nurun_mx", "Mind@123");
                SmtpServer.Host = "DMIT-111-0114";
                SmtpServer.Port = 587;
                SmtpServer.EnableSsl = false;
                //mail.Attachments.Add(new System.Net.Mail.Attachment(fichero));
                SmtpServer.Send(mail);
                return "success";
            } catch (Exception ex) {
                return ex.Message;
            }
        }

        public static string tableEmail(string body, cs_ObjURLImage url, string beforTable) {

            var table = @"<table width='705' height='636' border='0' bgcolor='#037EC1' cellpadding='0' cellspacing='0'>
                            <tr>
                                <td style='text-align: left'>&nbsp;</td>
                                <td style='text-align: left'><img src='http://www.eilor.mx/img/logo_kelloggs.png' alt='' /></td>
                                <td width='289' align='left' valign='middle' style='text-align: right'><img src='http://www.eilor.mx/img/logo_guadalajara.png' alt='' /></td>
                                <td width='23' align='left' valign='middle' style='text-align: right'>&nbsp;</td>
                            </tr>
                            <tr>
                                <td width='15' rowspan='3'>&nbsp;</td>
                                <td width='378' rowspan='3'>" + body + @"</td>
                                <td colspan='2'>&nbsp;</td>
                            </tr>
                            <tr>
                                <td height='58' colspan='2'>&nbsp;</td>
                            </tr>
                            <tr>
                                <td height='363' colspan='2'><img src='http://www.eilor.mx/img/tono.png' alt='' /></td>
                            </tr>
                        </table>";

            var tbl = table.Replace("'", "\"");

            return table.Replace("'","\"");
        }

    }
}