﻿using FarmaciasGuadalajara.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FarmaciasGuadalajara.AppObjects {
    public class cs_Admin {

        private long AdminId { get; set; }
        private string Name { get; set; }

        public cs_Admin() {
        }

        public cs_Admin(long AdminId, string Name) {
            this.AdminId = AdminId;
            this.Name = Name;
        }

        public Object getData() {
            FGEntities db = new FGEntities();
            try {
                return (Object)HttpContext.Current.Session["Admin"];
            } catch (Exception ex) {
                return null;
            }
        }

        public long getId() {
            try {
                return ((cs_Admin)HttpContext.Current.Session["Admin"]).AdminId;
            } catch (Exception ex) {
                return 0;
            }
        }

        public string getName() {
            try {
                return ((cs_Admin)HttpContext.Current.Session["Admin"]).Name;
            } catch (Exception ex) {
                return "";
            }
        }

    }

    public class MenuAdmin {
        
        public List<Menuelement> Menuelement { get; set; }

        public MenuAdmin() {            
            FGEntities db = new FGEntities();
            var resul = db.sp_MenuGet(Convert.ToInt32(new cs_Admin().getId())).First();                
            this.Menuelement = JArray.Parse(resul.message.ToString()).ToObject<List<Menuelement>>();
        }

    }

    public class Menuelement {
        public string Name { get; set; }
        public string Descripcion { get; set; }
        public string URL { get; set; }
        public List<string> Options { get; set; }
    }

    public class MenuelementOptions {
        public string Name { get; set; }
    }

}