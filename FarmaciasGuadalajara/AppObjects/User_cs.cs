﻿using FarmaciasGuadalajara.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FarmaciasGuadalajara.AppObjects {

    public class UserCl {

        private long UserId { get; set; }
        private string Name { get; set; }

        public UserCl() {
        }

        public UserCl(long AdminId, string Name) {
            this.UserId = AdminId;
            this.Name = Name;
        }

        public Object getData() {
            FGEntities db = new FGEntities();
            try {
                return (Object)HttpContext.Current.Session["User"];
            } catch (Exception ex) {
                return null;
            }
        }

        public long getId() {
            try {
                return ((UserCl)HttpContext.Current.Session["User"]).UserId;
            } catch (Exception ex) {
                return 0;
            }
        }

        public string getName() {
            try {
                return ((UserCl)HttpContext.Current.Session["User"]).Name;
            } catch (Exception ex) {
                return "";
            }
        }

    }

}