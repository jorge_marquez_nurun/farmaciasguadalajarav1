﻿using FarmaciasGuadalajara.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FarmaciasGuadalajara.AppObjects {
    public class cs_Client {

        public long ClientId { get; set; }
        public string Name { get; set; }
        public string LastName1 { get; set; }
        public string LastName2 { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }

        public cs_Client() {
        }

        public cs_Client(Cat_Client client) {
            this.ClientId = client.ClientId;
            this.Name = client.Name;
            this.LastName1 = client.LastName1;
            this.LastName2 = client.LastName2;
            this.Email = SimpleEncryption.Decrypt(client.Email, "FG");
            this.Phone = SimpleEncryption.Decrypt(client.LadaPhone, "FG")+" "+SimpleEncryption.Decrypt(client.Phone, "FG");
        }

        public Object getData() {
            FGEntities db = new FGEntities();
            try {
                return (Object)HttpContext.Current.Session["Client"];
            } catch (Exception ex) {
                return null;
            }
        }

        public long getId() {
            try {
                return ((cs_Client)HttpContext.Current.Session["Client"]).ClientId;
            } catch (Exception ex) {
                return 0;
            }
        }

        public string getName() {
            try {
                return ((cs_Client)HttpContext.Current.Session["Client"]).Name;
            } catch (Exception ex) {
                return "";
            }
        }

        public string getLastName1() {
            try {
                return ((cs_Client)HttpContext.Current.Session["Client"]).LastName1;
            } catch (Exception ex) {
                return "";
            }
        }

        public string getLastName2() {
            try {
                return ((cs_Client)HttpContext.Current.Session["Client"]).LastName2;
            } catch (Exception ex) {
                return "";
            }
        }

        public string getEmail() {
            try {
                return ((cs_Client)HttpContext.Current.Session["Client"]).Email;
            } catch (Exception ex) {
                return "";
            }
        }

        public string getPhone() {
            try {
                return ((cs_Client)HttpContext.Current.Session["Client"]).Phone;
            } catch (Exception ex) {
                return "";
            }
        }

    }

    public class Menu {
        public string menu { get; set; }

        public Menu(int type) {
            //If not Logged
            this.menu = "<li id=\"Login\" title=\"Iniciar sesión\" data-title=\"Iniciar sesión\"><a href=\"#\">Iniciar Sesión</a></li>" +
                        "<li id=\"Register\" title=\"Registro\" data-title=\"Registro\"><a href=\"#\">Registro</a></li>";
            // If Logged
            if (type == 1) {
                this.menu = "<li id=\"GoToStart\" title=\"Inicio\"><a href=\"#\">Inicio</a></li>" +
                            "<li id=\"MyAccount\" title=\">Mi perfil\" data-title=\">Mi perfil\"><a href=\"#\">Mi perfil</a></li>" +
                             //"<li id=\"RecoveryPassword\" title=\"Recuperar Contraseña\"><a href=\"#\">Recuperar Contraseña</a></li>" +
                             "<li class=\"logOut\" id=\"Logout\" title=\"Cerrar sesión\" data-title=\"Cerrar sesión\"><a href=\"#\">Cerrar sesión</a></li>"
                             // +"<li id=\"ChristmasTest\" title=\"Test Navideño\"><a href=\"/Main/ChristmasTest\">Test Navideño</a></li>" +
                             //"<li id=\"Award\"><a href=\"/Main/Award\" title=\"Premios\">Premios</a></li>" +
                             //"<li id=\"Winners\"><a href=\"/Main/Winners\" title=\"Ganadores\">Ganadores</a></li>"
                             ;
            }
        }

    }

    public class cs_Winners {

        public Int32 Lugar { get; set; }
        public string Nombre { get; set; }
        public Int32 Aciertos { get; set; }
        public string Tiempo { get; set; }
        public Int32 TotalPage { get; set; }

    }

}