﻿using CloudinaryDotNet;
using CloudinaryDotNet.Actions;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;

namespace FarmaciasGuadalajara.AppObjects {
    public class cs_ProcessImage {

        private string pathTemp { get; set; }
            private string path { get; set; }
            private int min_width { get; set; }
            private int max_width { get; set; }
            private int min_height { get; set; }
            private int max_height { get; set; }
            private int width { get; set; }
            private int height { get; set; }
            private string name { get; set; }
            private string extension { get; set; }

            public cs_ProcessImage(int[] width, int[] height) {
                this.name = "SYSAc_" + Guid.NewGuid().ToString();
                this.extension = "jpg";
                this.min_width = width[0];
                this.max_width = width[1];
                this.min_height = height[0];
                this.max_height = height[1];
                this.pathTemp = System.Web.HttpContext.Current.Server.MapPath("/Upload/images/temp_upload/");
                this.path = System.Web.HttpContext.Current.Server.MapPath("/Upload/images/");
            }

            public void setfile(string file) {
                this.path = System.Web.HttpContext.Current.Server.MapPath("/Upload/" + file + "/");
            }

            public void setnameInit(string name, bool fullName) {
                this.name = name + "_" + Guid.NewGuid().ToString();
                if (fullName) {
                    this.name = name;
                }
            }

            public void setExtension(string extension) {
                this.extension = extension;
            }

            public cs_Result uploadImage(HttpPostedFile img) {
                
                var err = "";

                try {

                    System.Drawing.Image imgDimension = System.Drawing.Image.FromStream(img.InputStream);
                    var ext = Path.GetExtension(img.FileName).ToLower();
                    ArrayList extensiones = new ArrayList();
                    extensiones.Add(".png");
                    extensiones.Add(".jpg");
                    extensiones.Add(".jpeg");
                    extensiones.Add(".gif");

                    if (!extensiones.Contains(ext)) {
                        return new cs_Result(1, "Formato invalido, el formato debe se alguno de los siguientes: \"png, jpg, jpeg, gif\"");
                    }

                    ////if (!Enumerable.Range(min_width, max_width).Contains(imgDimension.Width) || !Enumerable.Range(min_height, max_height).Contains(imgDimension.Height)) {
                    //if (imgDimension.Width < this.min_width || imgDimension.Height < this.min_height) {
                    //    return new ObjectResult(1, @"Las dimensiones de la imagen son incorrectas, las dimensiones deben de estar en un rando de:<br>
                    //                                " + this.min_width + "x" + this.min_height + " a " + this.max_width + " x " + this.max_height);
                    //}

                    //if (imgDimension.Width > this.max_width || imgDimension.Height > this.max_height) {
                    //    if (imgDimension.Width - this.max_width > 100 || imgDimension.Height - this.max_height > 100) {
                    //        return new ObjectResult(1, @"Las dimensiones de la imagen son incorrectas, las dimensiones deben de estar en un rando de:<br>
                    //                                " + this.min_width + "x" + this.min_height + " a " + this.max_width + " x " + this.max_height);
                    //    }
                    //}

                    this.width = imgDimension.Width;
                    this.height = imgDimension.Height;
                    //if (Enumerable.Range(min_width, max_width).Contains(imgDimension.Width) && Enumerable.Range(min_height, max_height).Contains(imgDimension.Height)) {
                    if (imgDimension.Width > this.max_width || imgDimension.Height > this.max_height) {
                        this.width = this.max_width;
                        this.height = this.max_height;
                    }

                    if (ext == ".gif") { this.extension = "gif"; }

                    this.pathTemp = this.pathTemp + this.name + "." + this.extension;
                    this.path = this.path + this.name + "." + this.extension;

                    err = err + " | " + this.pathTemp;
                    err = err + " | " + this.path;

                    img.SaveAs(this.pathTemp);
                    
                    err = err + " | save";

                    Cloudinary cloudinary = new Cloudinary(new Account("draycvsiz", "325897993887476", "dec87FUm8TXrDBfSDWu6_NI7OKA"));

                    var uploadParams = new ImageUploadParams() {
                        File = new FileDescription(this.pathTemp),
                        PublicId = this.name,
                        Transformation = new Transformation().FetchFormat(this.extension).Quality(85).Width(this.width).Height(this.height).Crop("mfit").Flags("progressive")
                    };
                    var uploadResult = cloudinary.Upload(uploadParams);

                    var url = uploadResult.Uri.Segments;

                    if (uploadResult.StatusCode.ToString() == "OK") {
                        WebClient webClient = new WebClient();
                        webClient.DownloadFile("http://res.cloudinary.com/" + url[1] + "/" + url[2] + url[3] + "/q_85/" + url[4] + url[5], this.path);
                        //System.IO.File.Delete(this.pathTemp);

                    } else {
                        return new cs_Result(1, "Formato invalido, el formato debe se alguno de los siguientes: \"png, jpg, jpeg, gif\"");
                    }

                    return new cs_Result(0, url[5]);

                } catch (Exception ex) {
                    err = err + " | " + ex.Message;
                    return new cs_Result(1, err);
                }

            }

    }
}