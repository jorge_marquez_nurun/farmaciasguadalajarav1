﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FarmaciasGuadalajara.AppObjects {
    public class cs_Result {

        public int code { get; set; }
        public string message { get; set; }

        public cs_Result() {
        }

        public cs_Result(int code, string message) {
            this.code = code;
            this.message = message;
        }

    }
}