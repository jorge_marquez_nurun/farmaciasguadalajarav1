﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FarmaciasGuadalajara.Models;
using System.Globalization;
using System.ComponentModel.DataAnnotations;

namespace FarmaciasGuadalajara.AppObjects {
    public class cs_Question {

        public cs_Question() {
        }

        public cs_Question(List<QuestionElement> question) {
            HttpContext.Current.Session["Question"] = question;
        }

        public QuestionElement questionSearch() {
            QuestionElement result = new QuestionElement();
            foreach (var element in (List<QuestionElement>)HttpContext.Current.Session["Question"]) {
                if (element.answerId == 0) {
                    HttpContext.Current.Session["questionId"] = element.questionId;
                    return element;
                }
            }
            return result;
        }

        public QuestionElement questionAnswer(Int32 answerId) {
            
            FGEntities db = new FGEntities();
            
            QuestionElement result = new QuestionElement();
            var Fecha = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss.FFFFFFF");
            var lista = ((List<QuestionElement>)HttpContext.Current.Session["Question"]);
            (lista.Find(a => a.questionId == Convert.ToInt32(HttpContext.Current.Session["questionId"])).answerId) = answerId;
            (lista.Find(a => a.questionId == Convert.ToInt32(HttpContext.Current.Session["questionId"])).Time) = Convert.ToDateTime(db.sp_getDate().First());

            HttpContext.Current.Session["Question"] = lista;

            foreach (var element in (List<QuestionElement>)HttpContext.Current.Session["Question"]) {
                if (element.answerId == 0) {
                    HttpContext.Current.Session["questionId"] = element.questionId;
                    return element;
                }
            }

            if (result.questionId == 0) {
                var cl = new cs_Client().getId();
                foreach (var element in (List<QuestionElement>)HttpContext.Current.Session["Question"]) {
                    db.sp_transactionInsert(0, new cs_Client().getId(), Convert.ToInt32(HttpContext.Current.Session["TicketId"]), element.questionId, element.answerId, element.Time, 0, "").First();
                }
            }

            HttpContext.Current.Session["TicketId"] = null;
            HttpContext.Current.Session["Question"] = null;
            HttpContext.Current.Session["questionId"] = null;

            return result;
        }

    }

    public class QuestionElement {

        public Int32 questionId { get; set; }
        public string questionName { get; set; }
        public List<Answer> data { get; set; }
        public Int32 answerId { get; set; }
        public DateTime Time { get; set; }

    }

    public class Answer {
        public Int32 answerId { get; set; }
        public string answerName { get; set; }
    }

}