﻿using System.Web;
using System.Web.Optimization;

namespace FarmaciasGuadalajara {
    public class BundleConfig {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles) {
            
            /****************************************************************
             * General
             ***************************************************************/

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                      "~/Scripts/bootstrap.js",
                       "~/Scripts/general.js",
                       "~/Scripts/jquery.maskedinput.js"));

            bundles.Add(new StyleBundle("~/Content/bootstrap").Include(
                       "~/Content/bootstrap.css",
                       "~/Content/site.css",
                       "~/Content/general.css"));

            bundles.Add(new StyleBundle("~/Content/bootstrap_cms").Include(
                       "~/Content/bootstrap.css",
                       "~/Content/general.css"));

            /****************************************************************
             * Only by main
             ***************************************************************/
            bundles.Add(new ScriptBundle("~/bundles/index_main").Include(
                       "~/Scripts/jquery.maskedinput.js",
                       "~/Scripts/index_main.js"));
            
            /*-------------  christmastest -------------*/
            bundles.Add(new ScriptBundle("~/bundles/christmastest").Include(
                        "~/Scripts/christmasTest.js"));

            /*-------------  profile -------------*/
            bundles.Add(new ScriptBundle("~/bundles/profile").Include(
                       "~/Scripts/profile.js"));

            /*-------------  register -------------*/
            bundles.Add(new ScriptBundle("~/bundles/register").Include(
                       "~/Scripts/register.js"));

            /****************************************************************
             * Only by CMS
             ***************************************************************/
            /*-------------  login -------------*/
            bundles.Add(new ScriptBundle("~/bundles/j0enHdx4Edx_cms_js").Include(
                      "~/Scripts/j0enHdx4Edx_cms.js"));

            bundles.Add(new StyleBundle("~/Content/j0enHdx4Edx_cms_css").Include(
                       "~/Content/admin_bootstrap.min.css",
                       "~/Content/AdminLTE.css"));

            /*-------------  clientList -------------*/
            bundles.Add(new ScriptBundle("~/bundles/clientList").Include(
                      "~/Scripts/jquery.twbsPagination.js",
                      "~/Scripts/clientList.js"));

            /*-------------  clientList -------------*/
            bundles.Add(new ScriptBundle("~/bundles/bestresults").Include(
                      "~/Scripts/jquery.twbsPagination.js",
                      "~/Scripts/best_results.js"));

        }
    }
}
